package com.example.admin.sematecandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class OriginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin);


        findViewById(R.id.goTo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent destinationIntent = new Intent(OriginActivity.this, DestinationActivity.class);
          startActivity(destinationIntent);

            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onresume", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onpause", Toast.LENGTH_SHORT).show();
    }
}
